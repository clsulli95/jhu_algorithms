package com.Project0.AlgorithmFactory;

import com.Project0.Point;

import java.util.ArrayList;

public class AlgorithmFactory {
    public Algorithm GetAlgorithm(AlgorithmType algorithmType, ArrayList<Point> list) {

        Algorithm retVal = null;

        switch (algorithmType) {
            case BRUTE_FORCE -> retVal = new BruteForce(list);
            case MORE_EFFICIENT -> retVal = new MoreEfficient(list);
            default -> System.out.println("Unsupported AlgorithmType selected!");
        }

        return retVal;
    }
}
