package com.Project0.AlgorithmFactory;
import com.Project0.Logger;
import com.Project0.PairOfPoints;
import com.Project0.Point;
import com.Project0.Project0Config;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class MoreEfficient implements Algorithm {

    private ArrayList<Point> m_List;
    private ArrayList<PairOfPoints> m_ClosestPoints;
    private Double m_CurrentClosestPairDistance;

    public MoreEfficient(ArrayList<Point> list)
    {
        m_List = list;
        m_ClosestPoints = new ArrayList<PairOfPoints>();
        m_CurrentClosestPairDistance = Double.POSITIVE_INFINITY;
    }

    private void Sort()
    {
        Collections.sort(m_List);
    }

    @Override
    public PairOfPoints FindClosestPair() {
        Sort();
        return FindClosestDistance(m_List);
    }

    @Override
    public PairOfPoints GetClosestPair()
    {
        PairOfPoints closestPair = null;

        if (m_ClosestPoints.size() > 0)
        {
            closestPair = m_ClosestPoints.get(0);
        }
        else
        {
            Logger.Log("[MoreEfficient][GetClosestPoint()] No Closest Point Found!");
        }
        return closestPair;
    }

    @Override
    public ArrayList<PairOfPoints> GetClosestPairs()
    {
        ArrayList<PairOfPoints> noDupes = new ArrayList<>();
        for (int index = 0; index < m_ClosestPoints.size(); index++)
        {
            if (!noDupes.contains(m_ClosestPoints.get(index)))
            {
                noDupes.add(m_ClosestPoints.get(index));
            }
        }
        return noDupes;
    }

    private PairOfPoints FindClosestDistance(ArrayList<Point> pointsList)
    {
        if (pointsList.size() <= Project0Config.BRUTE_FORCE_CONSTANT)
        {
            BruteForce bruteAlgo = new BruteForce(pointsList);
            PairOfPoints closestPair = bruteAlgo.FindClosestPair();
            return closestPair;
        }
        else
        {
            int midPoint = pointsList.size() / 2;
            ArrayList<Point> leftOfMidpoint = new ArrayList<Point>(pointsList.subList(0, midPoint));
            ArrayList<Point> rightOfMidpoint = new ArrayList<Point>(pointsList.subList(midPoint, pointsList.size()));
            PairOfPoints leftOfMidpointSmallestDistance = FindClosestDistance(leftOfMidpoint);
            PairOfPoints rightOfMidpointSmallestDistance = FindClosestDistance(rightOfMidpoint);

            PairOfPoints minimumOfBothSides = PairOfPoints.min(leftOfMidpointSmallestDistance, rightOfMidpointSmallestDistance);

            // Construct Split and Brute Force Split
            ArrayList<Point> split = new ArrayList<Point>();

            for (int index = 0; index < leftOfMidpoint.size(); index++)
            {
                if(Math.abs(pointsList.get(midPoint).GetX() - leftOfMidpoint.get(index).GetX()) < minimumOfBothSides.GetDistance())
                {
                    split.add(leftOfMidpoint.get(index));
                }
            }

            for (int index = 0; index < rightOfMidpoint.size(); index++)
            {
                if (Math.abs(pointsList.get(midPoint).GetX() - rightOfMidpoint.get(index).GetX()) < minimumOfBothSides.GetDistance())
                {
                    split.add(rightOfMidpoint.get(index));
                }
            }

            BruteForce splitBruteForce = new BruteForce(split);
            PairOfPoints minimumOfSplit = splitBruteForce.FindClosestPair();

            PairOfPoints minimumOfSidesAndSplit = PairOfPoints.min(minimumOfBothSides, minimumOfSplit);
            AddToListOfClosestPoints(minimumOfSidesAndSplit);

            return minimumOfSidesAndSplit;
        }
    }

    private void AddToListOfClosestPoints(PairOfPoints closestPair)
    {
        if (m_ClosestPoints.size() == 0)
        {
            m_CurrentClosestPairDistance = closestPair.GetDistance();
            m_ClosestPoints.add(closestPair);
        }
        else
        {
            if (closestPair.GetDistance() == m_CurrentClosestPairDistance)
            {
                m_ClosestPoints.add(closestPair);
            }
            else if (closestPair.GetDistance() < m_CurrentClosestPairDistance)
            {
                m_ClosestPoints.clear();
                m_CurrentClosestPairDistance = closestPair.GetDistance();
                m_ClosestPoints.add(closestPair);
            }
        }
    }
}
