package com.Project0.AlgorithmFactory;

public enum AlgorithmType {
    NONE,
    BRUTE_FORCE,
    MORE_EFFICIENT
}
