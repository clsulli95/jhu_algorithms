package com.Project0.AlgorithmFactory;
import com.Project0.PairOfPoints;
import com.Project0.Point;

import java.util.ArrayList;

public interface Algorithm {
    abstract PairOfPoints FindClosestPair();
    abstract PairOfPoints GetClosestPair();
    abstract ArrayList<PairOfPoints> GetClosestPairs();
}
