package com.Project0.AlgorithmFactory;
import com.Project0.Point;
import com.Project0.Project0Config;

public class Formulas {
    public static double EuclideanDistance(Point point1, Point point2)
    {
        // (X1-X2)^2
        double xCoords = Math.pow((point1.GetX() - point2.GetX()), Project0Config.SQUARED);
        // (Y1-Y2)^2
        double yCoords = Math.pow((point1.GetY() - point2.GetY()), Project0Config.SQUARED);
        // sqrt( (X1-X2)^2 + (Y1-Y2)^2 )
        double distance = Math.sqrt(xCoords + yCoords);

        return distance;
    }
}
