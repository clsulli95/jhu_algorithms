package com.Project0.AlgorithmFactory;

import com.Project0.PairOfPoints;
import com.Project0.Point;

import java.util.ArrayList;

public class BruteForce implements Algorithm {

    private ArrayList<Point> m_List;
    private ArrayList<PairOfPoints> m_ClosestPoints;
    private Double m_CurrentClosestPairDistance;


    public BruteForce(ArrayList<Point> listToSort)
    {
        m_List = listToSort;
        m_ClosestPoints = new ArrayList<PairOfPoints>();
        m_CurrentClosestPairDistance = Double.POSITIVE_INFINITY;
    }

    @Override
    public PairOfPoints FindClosestPair()
    {
        Double localMinimum = Double.POSITIVE_INFINITY;
        PairOfPoints closestPair = null;

        if (m_List.size() > 1)
        {
            for (int indexOuter = 0; indexOuter < m_List.size(); indexOuter++)
            {
                for (int indexInner = indexOuter + 1; indexInner < m_List.size(); indexInner++)
                {
                    localMinimum = DetermineDistance(m_List.get(indexOuter), m_List.get(indexInner));

                    if (localMinimum <= m_CurrentClosestPairDistance)
                    {
                        Point closestPoint1 = new Point(m_List.get(indexOuter));
                        Point closestPoint2 = new Point(m_List.get(indexInner));
                        closestPair = new PairOfPoints(closestPoint1, closestPoint2, localMinimum);
                        AddToListOfClosestPoints(closestPair);
                    }
                }
            }

            return closestPair;
        }
        else
        {
            closestPair = new PairOfPoints(new Point(), new Point(), Double.POSITIVE_INFINITY);
            return closestPair;
        }
    }

    @Override
    public PairOfPoints GetClosestPair()
    {
        return m_ClosestPoints.get(0);
    }

    @Override
    public ArrayList<PairOfPoints> GetClosestPairs()
    {
        return m_ClosestPoints;
    }

    private double DetermineDistance(Point point1, Point point2)
    {
        double distance = Formulas.EuclideanDistance(point1, point2);
        return distance;
    }

    private void AddToListOfClosestPoints(PairOfPoints closestPair)
    {
        if (closestPair.GetDistance() == m_CurrentClosestPairDistance)
        {
            m_ClosestPoints.add(closestPair);
        }
        else if (closestPair.GetDistance() < m_CurrentClosestPairDistance)
        {
            m_ClosestPoints.clear();
            m_ClosestPoints.add(closestPair);
            m_CurrentClosestPairDistance = closestPair.GetDistance();
        }
    }

}
