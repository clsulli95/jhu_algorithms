package com.Project0;

public class Project0Config {
    public static Boolean DEBUG = false;
    public static int TRACE_SIZE_100 = 100;
    public static int TRACE_SIZE_1K = 1000;
    public static int TRACE_SIZE_10K = 10000;
    public static int TRACE_SIZE_100K = 100000;
    public static int TRACE_SIZE_500K = 500000;
    public static int TRACE_SIZE_1M = 1000000;
    public static String TRACE_FILE_100 = "Input100.txt";
    public static String TRACE_FILE_1K = "Input1000.txt";
    public static String TRACE_FILE_10K = "Input10000.txt";
    public static String TRACE_FILE_100K = "Input100000.txt";
    public static String TRACE_FILE_500K = "Input500000.txt";
    public static String TRACE_FILE_1M = "Input1000000.txt";
    public static int TRACE_RUNTIME_SIZE = 1000000;
    public static int LOWER_BOUND = -10000;
    public static int UPPER_BOUND = 10000;
    public static int INCLUSIVE_OFFSET = 1;
    public static int SQUARED = 2;
    public static int BRUTE_FORCE_CONSTANT = 3;
}
