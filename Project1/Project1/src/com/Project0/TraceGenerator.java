package com.Project0;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Scanner;
import java.io.*;

public class TraceGenerator {

    public TraceGenerator() {

    }

    public ArrayList<Point> GenerateTrace(int traceSize) {
        ArrayList<Point> listOfPoints = new ArrayList<Point>();

        File theTraceFile = CreateTraceFile(traceSize);

        for (int index = 0; index < traceSize; index++) {
            Point randomPoint = GenerateRandomPair(Project0Config.LOWER_BOUND, Project0Config.UPPER_BOUND);
            if (!listOfPoints.contains(randomPoint))
            {
                listOfPoints.add(randomPoint);
            }
        }

        try
        {
            FileWriter traceFileWriter = new FileWriter(theTraceFile.getName());
            for (int index = 0; index < listOfPoints.size(); index++)
            {
                traceFileWriter.write(listOfPoints.get(index).toFile());
            }
            traceFileWriter.close();;
        }
        catch (IOException e) {
            System.out.println(String.format("Error writing to file! Name: %s", theTraceFile.getName()));
        }

        return listOfPoints;
    }

    public ArrayList<Point> ConsumeInputFile(String inputFile)
    {
        ArrayList<Point> pointsList = new ArrayList<Point>();
        try
        {
            Scanner fileScanner = new Scanner(new File(inputFile));

            while (fileScanner.hasNextLine())
            {
                Point pointToAdd = new Point(fileScanner.nextLine());
                pointsList.add(pointToAdd);
            }
            fileScanner.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Error Reading File!");
        }

        return pointsList;
    }

    private Point GenerateRandomPair(int lowerBound, int upperBound)
    {
        int randomX = ThreadLocalRandom.current().nextInt(lowerBound, upperBound + Project0Config.INCLUSIVE_OFFSET);
        int randomY = ThreadLocalRandom.current().nextInt(lowerBound, upperBound + Project0Config.INCLUSIVE_OFFSET);

        Point randomPoint = new Point(randomX, randomY);

        return randomPoint;
    }

    private File CreateTraceFile(int traceSize)
    {
        System.out.println("Creating File!");
        File traceFile = new File(String.format("Input%d.txt", traceSize));
        return traceFile;
    }
}
