package com.Project0;

public class PairOfPoints {
    private Point m_Pair1;
    private Point m_Pair2;
    private Double m_Distance;

    public PairOfPoints(Point pair1, Point pair2, Double distance)
    {
        m_Pair1 = pair1;
        m_Pair2 = pair2;
        m_Distance = distance;
    }

    public Double GetDistance()
    {
        return m_Distance;
    }

    public static PairOfPoints min(PairOfPoints pair1, PairOfPoints pair2)
    {
        if (Math.min(pair1.m_Distance, pair2.m_Distance) == pair1.m_Distance)
        {
            return pair1;
        }
        else if(Math.min(pair1.m_Distance, pair2.m_Distance) == pair2.m_Distance)
        {
            return pair2;
        }
        else
        {
            Logger.Log("Error calculating Minimum between Pairs!");
            return null;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this)
        {
            return true;
        }
        if (o == null)
        {
            return false;
        }

        PairOfPoints pairOfPointsToCompare = (PairOfPoints) o;

        if (pairOfPointsToCompare.m_Pair1 == this.m_Pair1 &&
            pairOfPointsToCompare.m_Pair2 == this.m_Pair2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String toString()
    {
        return String.format("P1: %s P2: %s Distance: %f", m_Pair1.toString(), m_Pair2.toString(), m_Distance);
    }
}
