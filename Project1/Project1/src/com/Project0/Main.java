package com.Project0;
import com.Project0.AlgorithmFactory.Algorithm;
import com.Project0.AlgorithmFactory.AlgorithmFactory;
import com.Project0.AlgorithmFactory.AlgorithmType;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // The TraceGenerator in charge of generating Trace files and
        // populating the input ArrayList<Point> from Trace files.
        TraceGenerator traceGenerator = new TraceGenerator();

        // To Generate new trace files, use the TraceGenerator.GenerateTrace
        // function and pass in the size of the trace file you would like to
        // generate.  Constants for predetermined Trace File sizes exist
        // within Project0Config.java.
        // Ex. traceGenerator.GenerateTrace(Project0Config.TRACE_SIZE_1K)

        // The AlgorithmFactory is in charge of providing the Alogrithm
        // object that is requested by the user.  For now just BRUTE_FORCE
        // and MORE_EFFICIENT are available.
        AlgorithmFactory algorithmFactory = new AlgorithmFactory();

        // BRUTE_FORCE Section
        System.out.println("--------------------- BRUTE FORCE -----------------------------");
        ArrayList<Point> tracePairsBrute = traceGenerator.ConsumeInputFile(Project0Config.TRACE_FILE_100);
        Algorithm algoBruteForce = algorithmFactory.GetAlgorithm(AlgorithmType.BRUTE_FORCE, tracePairsBrute);
        PairOfPoints closestPairDistanceBrute = algoBruteForce.FindClosestPair();
        ArrayList<PairOfPoints> closestPairsDistanceBrute = algoBruteForce.GetClosestPairs();
        System.out.println(String.format("Closest Pair: %s", closestPairDistanceBrute));
        System.out.println("Closest Pairs are: ");
        for (int index = 0; index < closestPairsDistanceBrute.size(); index++)
        {
            System.out.println(String.format("   %s", closestPairsDistanceBrute.get(index)));
        }
        System.out.println("--------------------- -------------- -----------------------------");

        System.out.println("--------------------- MORE EFFICIENT -----------------------------");
        ArrayList<Point> tracePairsEfficient = traceGenerator.ConsumeInputFile(Project0Config.TRACE_FILE_100);
        Algorithm algoMoreEfficient = algorithmFactory.GetAlgorithm(AlgorithmType.MORE_EFFICIENT, tracePairsEfficient);
        PairOfPoints closestPairDistanceEfficient = algoMoreEfficient.FindClosestPair();
        ArrayList<PairOfPoints> closestPairsDistanceEfficient = algoMoreEfficient.GetClosestPairs();
        System.out.println(String.format("Smallest Distance MORE_EFFICIENT: %s", closestPairDistanceEfficient));
        System.out.println("Closest Pairs are: ");
        for (int index = 0; index < closestPairsDistanceEfficient.size(); index++)
        {
            System.out.println(String.format("   %s", closestPairsDistanceEfficient.get(index)));
        }
        System.out.println("--------------------- -------------- -----------------------------");




    }
}
