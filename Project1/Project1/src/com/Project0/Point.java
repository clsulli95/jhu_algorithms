package com.Project0;

import java.net.Inet4Address;
import java.util.Arrays;

public class Point implements Comparable<Point>{
    private int m_X;
    private int m_Y;

    public Point()
    {
        m_X = Integer.MAX_VALUE;
        m_Y = Integer.MAX_VALUE;
    }

    public Point(int xCoordinate, int yCoordinate) {
        m_X = xCoordinate;
        m_Y = yCoordinate;
    }

    public Point(String pointString)
    {
        String[] splitString = pointString.split(" ");
        m_X = Integer.parseInt(splitString[0]);
        m_Y = Integer.parseInt(splitString[1]);
    }

    public Point(Point pointToCopy)
    {
        m_X = pointToCopy.m_X;
        m_Y = pointToCopy.m_Y;
    }

    public int GetX() {
        return m_X;
    }

    public int GetY(){
        return m_Y;
    }

    public void SetX(int xCoordinate){
        m_X = xCoordinate;
    }

    public void SetY(int yCoordinate) {
        m_Y = yCoordinate;
    }

    public String toString()
    {
        return String.format("(%d, %d)", m_X, m_Y);
    }

    public String toFile() {
        return String.format("%d %d\n", m_X, m_Y);
    }
    @Override
    public int compareTo(Point o) {
        return Double.compare(GetX(), o.GetX());
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this)
        {
            return true;
        }
        if (o == null)
        {
            return false;
        }

        Point pointToCompare = (Point) o;

        if (pointToCompare.m_X == this.m_X && pointToCompare.m_Y == this.m_Y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
